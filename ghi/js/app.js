function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text-color{light grey}"><small class="text-body-secondary">${location}</small></p>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-body-secondary">
        ${startDate.toLocaleDateString()}-${endDate.toLocaleDateString()}
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);

        if (!response.ok) {
          return alert(
            `${response.status}: ${response.url} ${response.statusText}`
          );
        } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const startDate = new Date(details.conference.starts);
                startDate.toLocaleDateString();
                const endDate = new Date(details.conference.ends);
                endDate.toLocaleDateString();
                const location = details.conference.location.name;

                const html = createCard(name, description, pictureUrl, startDate, endDate, location);

                const row = document.querySelector('.row');
                row.innerHTML += html;
            }
        }}
    } catch (e) {
        console.error(e);
        alert(e.message);
    }
    function alert(message) {
      const alertPlaceholder = document.getElementById('liveAlertPlaceholder')
      const wrapper = document.createElement('div');
      wrapper.innerHTML =
        `<div class="alert alert-danger alert-dismissible" role="alert">,
           <div>${message}</div>,
           <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>,
        </div>`;

    alertPlaceholder.append(wrapper);
  }
});
