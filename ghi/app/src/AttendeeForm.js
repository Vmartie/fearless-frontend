import React, { useEffect, useState} from 'react';

function AttendeeForm() {
    const [conferences, setConferences] = useState([]);
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [conference, setConference] = useState("");

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.email = email;
        data.conference = conference;
        console.log(data);

        const conferenceUrl = 'http://localhost:8001/api/conferences/1/attendees/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch (conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            setName('');
            setEmail('');
            setConference('');
        }
    }
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/ ';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences)
            }
        }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>It's Conference Time!</h1>
          <p>
            <h6>Please choose which conference you'd like to attend.</h6>
          </p>
          <form onSubmit={handleSubmit} id="create-conference-form">
          <div className="mb-3">
            <select onChange={handleConferenceChange} value={conference} required id="conference" name="conference" className="form-select">
                <option value="">Choose a conference.</option>
                {conferences.map(conference => {
                    return (
                        <option key={conference.href} value={conference.href}>
                            {conference.name}
                        </option>
                    );
                })}
            </select>
            </div>
            <p>
                <h6>Now tell us about yourself.</h6>
            </p>
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
              <label htmlFor="name">Your full name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEmailChange} value={email} placeholder="Email" required type="text" name="email" id="email" className="form-control"/>
              <label htmlFor="email">Your full email</label>
            </div>
            <button className="btn btn-primary">I'm going!</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default AttendeeForm;
